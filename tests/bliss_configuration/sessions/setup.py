import random
from bliss.common.standard import *

# TODO: replace with current_session
# from bliss import get_current_session
import numpy
import gevent
from bliss.common.event import dispatcher
from bliss.scanning.scan_display import ScanDisplay
import math

# deactivate automatic Flint startup
ScanDisplay().auto = False

load_script("simple")

# SESSION_NAME = get_current_session().name

# Do not remove this print (used in tests)
print("TEST_SESSION INITIALIZED")
#

import pytest
import numpy
import random
from daiquiri.core.transform import metrics


def test_tensor():
    # Standard representation
    tensor = metrics.EuclideanMetricTensor([1, 0], [0, 1])
    calc = tensor([1, 2], [3, 4])
    expected = [[11]]
    numpy.testing.assert_array_equal(calc, expected)

    # Orthonormal basis (inner product is preserved)
    a = 0.5 ** 0.5
    tensor = metrics.EuclideanMetricTensor([a, a], [-a, a])
    calc = tensor([1, 2], [3, 4])
    expected = [[11]]
    numpy.testing.assert_almost_equal(calc, expected)

    # Convert to standard basis and back
    ve = [0, 1]
    vb = [0.5 ** 0.5, 0.5 ** 0.5]
    calc = tensor.to_standard(vb)
    expected = [ve]
    numpy.testing.assert_almost_equal(calc, expected)
    calc = tensor.from_standard(ve)
    expected = [vb]
    numpy.testing.assert_almost_equal(calc, expected)

    # Non-orthogonal basis (inner product is not preserved)
    tensor = metrics.EuclideanMetricTensor([2, 3], [4, 5])
    calc = tensor([1, 2], [3, 4])
    expected = [[11]]
    with pytest.raises(AssertionError):
        numpy.testing.assert_almost_equal(calc, expected)

    # Convert to standard basis and back
    vb = [[2, 0], [0, 3]]
    ve = [[4, 6], [12, 15]]
    calc = tensor.to_standard(vb)
    expected = ve
    numpy.testing.assert_array_equal(calc, expected)
    calc = tensor.from_standard(ve)
    expected = vb
    numpy.testing.assert_almost_equal(calc, expected)

    # Round-trip test
    for _ in range(100):
        ndim = random.randint(1, 5)
        nvec = random.randint(1, 5)
        vb = numpy.random.uniform(-10, 10, (ndim, ndim))
        tensor = metrics.EuclideanMetricTensor(*vb)
        v1 = numpy.random.uniform(-10, 10, (nvec, ndim))
        v2 = tensor.from_standard(tensor.to_standard(v1))
        numpy.testing.assert_almost_equal(v1, v2)
        v2 = tensor.to_standard(tensor.from_standard(v1))
        numpy.testing.assert_almost_equal(v1, v2)

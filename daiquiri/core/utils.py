#!/usr/bin/env python
# -*- coding: utf-8 -*-
import importlib
import threading
import time
import math
import logging
import inspect
import gevent
import cProfile
import pstats
from io import StringIO
from contextlib import contextmanager
from functools import wraps
import numpy as np
from pint import UnitRegistry
from copy import deepcopy

logger = logging.getLogger(__name__)
ureg = UnitRegistry()


@contextmanager
def profiler(sortby="cumtime", filename=None):
    """Wraps the context in a cProile instance

    Kwargs:
        sortby (str): sort time profile
        filename (str): filename to save to
    """
    pr = cProfile.Profile()
    pr.enable()
    try:
        yield
    finally:
        pr.disable()

        s = StringIO()
        ps = pstats.Stats(pr, stream=s)

        if filename:
            ps.dump_stats(filename)


def timed(fn):
    """Decorator to time function execution

    Should work with class an static methods
    """

    @wraps(fn)
    def wrapper(*args, **kwargs):
        params = inspect.signature(fn).parameters
        if params.get("self"):
            cln = args[0].__class__.__name__
        else:
            cln = "Static"

        start = time.time()
        ret = fn(*args, **kwargs)
        took = round((time.time() - start) * 1000)

        logger.debug(f"{cln}::{fn.__name__} took {took} ms")

        return ret

    return wrapper


def dict_nd_to_list(dct):
    """Recursively replace ndarray with list in a dictionary
    """
    recursed = {}

    for k, v in dct.items():
        if type(v) is dict:
            recursed[k] = dict_nd_to_list(v)
        else:
            if type(v) == np.ndarray:
                recursed[k] = v.tolist()
            else:
                recursed[k] = v

    return recursed


def debounce(wait):
    """ Decorator that will postpone a functions
        execution until after wait seconds
        have elapsed since the last time it was invoked. 
        https://gist.github.com/walkermatt/2871026
    """

    def decorator(fn):
        def debounced(*args, **kwargs):
            def call_it():
                debounced._timer = None
                debounced._last_call = time.time()
                return fn(*args, **kwargs)

            time_since_last_call = time.time() - debounced._last_call
            if time_since_last_call >= wait:
                return call_it()

            if debounced._timer is None:
                debounced._timer = threading.Timer(wait - time_since_last_call, call_it)
                debounced._timer.start()

        debounced._timer = None
        debounced._last_call = 0

        return debounced

    return decorator


def debounce_with_table(wait):
    last_call = {}

    def decorator(fn):
        def debounced(self, obj, prop, value):
            pid = obj.id() + prop

            def call_it(_pid):
                val = last_call[_pid]["value"]
                del last_call[_pid]
                return fn(self, obj, prop, val)

            if not (pid in last_call):
                timer = threading.Timer(wait, call_it, [pid])
                timer.start()
                last_call[pid] = {"timer": timer, "last_call": time.time()}
                last_call[pid]["value"] = value

            else:
                data = last_call[pid]
                time_since_last_call = time.time() - data["last_call"]
                last_call[pid]["value"] = value

                if time_since_last_call >= wait:
                    return call_it(pid)

        return debounced

    return decorator


# Thieved from http://code.activestate.com/recipes/577346-getattr-with-arbitrary-depth/
def get_nested_attr(obj, attr, **kw):
    attributes = attr.split(".")
    for i in attributes:
        try:
            obj = getattr(obj, i)
            if callable(obj):
                obj = obj()
        except AttributeError:
            if kw.has_key("default"):
                return kw["default"]
            else:
                raise
    return obj


def loader(base, postfix, module, *args, **kwargs):
    """Try loading class "{module.title()}{postfix}" from
    "{base}{module}" and instantiate it.

    For example "ExampleActor" from "daiquiri.implementors.examplecomponent".

    :param str base: base module
    :param str postfix: add to module name to get class name
    :param str module: submodule
    :param args: for class instantiation
    :param kwargs: for class instantiation
    :returns Any:
    """
    # Making sure that everything up to the last module
    # is treated as base module
    if "." in module:
        mod_parts = module.split(".")
        base = base + "." + ".".join(mod_parts[0:-1])
        module = mod_parts[-1]
    # Load class from module
    mod_file = base + "." + module
    class_name = module.title() + postfix
    try:
        mod = importlib.import_module(mod_file)
        mod = importlib.reload(mod)
    except ModuleNotFoundError:
        err_msg = "Couldn't find module {}".format(mod_file)
        logger.error(err_msg)
        raise
    # Import class
    try:
        cls = getattr(mod, class_name)
    except AttributeError:
        err_msg = "Couldn't import '{}' from {}".format(class_name, mod_file)
        logger.error(err_msg)
        raise
    # Instantiate class
    instance = cls(*args, **kwargs)
    msg = "Instantiated '{}' from {}".format(class_name, mod_file)
    logger.info(msg)
    return instance


def format_eng(scalar):
    power_prefix = {
        24: "Y",
        21: "Z",
        18: "E",
        15: "P",
        12: "T",
        9: "G",
        6: "M",
        3: "k",
        -3: "m",
        -6: "\u00B5",
        -9: "n",
        -12: "p",
        -15: "f",
        -18: "a",
        -21: "z",
        -24: "y",
    }

    q = math.log(scalar) / math.log(1e3)
    subtrhnd = not q.is_integer()

    pow_10 = 3 * math.ceil(q - subtrhnd)
    prefix = "" if pow_10 == 0 else power_prefix[pow_10]

    return {
        "scalar": scalar * math.pow(10, -pow_10),
        "prefix": prefix,
        "multiplier": math.pow(10, -pow_10),
    }


def get_start_end(kwargs, points=None, last=False, default_per_page=25):
    per_page = kwargs.get("per_page", default_per_page)
    page = kwargs.get("page", None)
    pages = None

    if points:
        pages = math.ceil(points / per_page)

    if page is None:
        if last:
            st = max(0, points - per_page)
            en = points
            page = pages

        else:
            st = 0
            en = per_page
            page = 1

    else:
        st = per_page * (page - 1)
        en = per_page + per_page * (page - 1)

    return {"st": st, "en": en, "page": page, "pages": pages, "per_page": per_page}


def to_wavelength(energy):
    return (
        ((ureg.h * ureg.c) / (energy * ureg.eV).to(ureg.J)).to(ureg.angstrom).magnitude
    )


def to_energy(wavelength):
    return ((ureg.h * ureg.c) / (wavelength * ureg.angstrom)).to(ureg.eV).magnitude


def make_json_safe(obj):
    """Make sure a dict is json encodable

    Converts np types to int, float, list

    Args:
        obj(dict): The dict to make safe
    """
    new_obj = {}
    for k, v in obj.items():
        new_obj[k] = _make_json_safe(v)

    return new_obj


def _make_json_safe(v):
    if isinstance(v, dict):
        return make_json_safe(v)

    elif isinstance(v, list):
        return [_make_json_safe(i) for i in v]

    elif isinstance(v, np.integer):
        return int(v)

    elif isinstance(v, np.float):
        return float(v)

    elif isinstance(v, np.ndarray):
        return v.tolist()

    elif isinstance(v, np.dtype):
        return str(v)

    elif isinstance(v, np.bool_):
        return bool(v)

    return v


def deep_diff(x, y, exclude_keys=[]):
    """Take the deep diff of JSON-like dictionaries

    https://stackoverflow.com/a/62124626
    No warranties when keys, or values are None
    """
    if x == y:
        return None

    if type(x) != type(y) or type(x) not in [list, dict]:
        return y  # (x, y)

    if type(x) == dict:
        d = {}
        for k in x.keys() ^ y.keys():
            if k in exclude_keys:
                continue
            if k in x:
                d[k] = (deepcopy(x[k]), None)
            else:
                d[k] = (None, deepcopy(y[k]))

        for k in x.keys() & y.keys():
            if k in exclude_keys:
                continue

            next_d = deep_diff(x[k], y[k])
            if next_d is None:
                continue

            d[k] = next_d

        return d if len(d) else None

    # must be list:
    d = [None] * max(len(x), len(y))
    flipped = False
    if len(x) > len(y):
        flipped = True
        x, y = y, x

    for i, x_val in enumerate(x):
        d[i] = deep_diff(y[i], x_val) if flipped else deep_diff(x_val, y[i])

    for i in range(len(x), len(y)):
        d[i] = (y[i], None) if flipped else (None, y[i])


def worker(fn):
    """Run a function in a real thread"""
    pool = gevent.get_hub().threadpool
    return pool.spawn(fn).get()

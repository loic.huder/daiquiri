# -*- coding: utf-8 -*-

import logging
import marshmallow

from marshmallow import fields, ValidationError, Schema, EXCLUDE
from daiquiri.core import CoreResource, marshal
from daiquiri.core.metadata import MetaDataHandler

from requests import Session
from requests.auth import HTTPBasicAuth  # or HTTPDigestAuth, or OAuth1, etc.
from zeep import Client
from zeep.transports import Transport

logger = logging.getLogger(__name__)


class ISPyBConfigSchema(Schema):
    meta_host = fields.URL()
    meta_user = fields.Str()
    meta_password = fields.Str()
    meta_type = fields.Str()
    beamline = fields.Str()


class ProposalResource(CoreResource):
    def get(self, code, number):
        """Get current proposal"""
        proposal = self._parent.find_proposal(code, number)

        if proposal:
            return proposal, 200
        else:
            return {"error": "No such propsoal"}, 404


class IspybMetaDataHandler(MetaDataHandler):
    def __init__(self, *args, **kwargs):
        self._config = ISPyBConfigSchema().load(
            kwargs.get("config", {}), unknown=EXCLUDE
        )
        super().__init__(*args, **kwargs)

    def setup(self, *args, **kwargs):
        logger.debug("Loaded: {c}".format(c=self.__class__.__name__))

        self.register_route(ProposalResource, "/proposal")
        host = self._config.get("meta_host")

        session = Session()
        session.auth = HTTPBasicAuth(
            self._config.get("meta_user"), self._config.get("meta_password")
        )

        self._shipping_client = Client(
            host + "ToolsForShippingWebService?wsdl",
            transport=Transport(session=session),
        )

        self._collection_client = Client(
            host + "ToolsForCollectionWebService?wsdl",
            transport=Transport(session=session),
        )

    def find_sessions(self, code, number):
        return self._collection_client.service.findSessionsByProposalAndBeamLine(
            code + number, self._config.get("beamline")
        )

    def find_proposal(self, code, number):
        return self._shipping_client.service.findProposal(code, number)

#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from daiquiri.core.saving.bliss_basic import Bliss_BasicSavingHandler

logger = logging.getLogger(__name__)


class Bliss_EsrfSavingHandler(Bliss_BasicSavingHandler):
    def _set_filename(self, proposal=None, sample=None, dataset=None):
        self.scan_saving.proposal = proposal
        self.scan_saving.sample = sample
        self.scan_saving.dataset = dataset

#!/usr/bin/env python
# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod
from marshmallow import Schema

import logging

logger = logging.getLogger(__name__)


class ParamHandler(ABC):
    """ Parameter Handler 
        
    Handler for an individual parameter, two methods should be implemented
    in the child, before, and after. These will be called before an actor is
    started and after an actor finishes.

    A handler can also store an arbitrary value so that for example a position
    can be restored after an actor finishes
    """

    def __init__(self):
        self._value = None

    def set_cache(self, value):
        self._value = value

    def get_cache(self):
        return self._value

    def do_before(self, value):
        logger.info(f"Running before {self.__class__.__name__}")
        self.before(value)
        logger.info(f"Before {self.__class__.__name__} finished")

    def do_after(self, value):
        logger.info(f"Running after {self.__class__.__name__}")
        self.after(value)
        logger.info(f"After {self.__class__.__name__} finished")

    def before(self, value):
        """ Implemented in child """
        pass

    def after(self, value):
        """ Implemented in child """
        pass


class ParamsHandlerMeta(type):
    """ Parameters Handler Metaclass

    This meta class pops attributes off the child instance into a handlers
    attribute so classes can be declared in a simple fashion
    """

    def __new__(mcs, name, bases, attrs):
        handlers = {}
        to_rem = []
        for a, v in attrs.items():
            if isinstance(v, ParamHandler):
                handlers[a] = v
                to_rem.append(a)

        for r in to_rem:
            del attrs[r]

        klass = super().__new__(mcs, name, bases, attrs)
        klass.handlers = handlers

        return klass


class ParamsHandler(metaclass=ParamsHandlerMeta):
    """ Parameters Handler 

    This class will find the parameter instance to maniplate from the
    registered parameters.
    """

    def handle(self, params, before=True):
        for p, v in params.items():
            if p in self.handlers:
                meth = getattr(self.handlers[p], "do_before" if before else "do_after")
                meth(v)


class ParamSchema(Schema):
    """ Parameteres Schema

    A special marshmallow schema that has `ParamsHandler` attribute in 
    addition to the schema. These handlers will be used if a schema contains 
    values from this ParamSchema
    """

    handler = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.Meta.cache = True

        if self.handler is not None:
            if not isinstance(self.handler, ParamsHandler):
                raise AttributeError(
                    "Parameters handler is not an instance of <ParamsHandler>"
                )

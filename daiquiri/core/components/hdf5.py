#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import logging
import h5py
from marshmallow import fields

from daiquiri.core import marshal
from daiquiri.core.components import Component, ComponentResource
from daiquiri.core.schema import ErrorSchema
from daiquiri.core.schema.metadata import paginated
from daiquiri.core.schema.components.hdf5 import RootHdf5Schema, Hdf5Schema
from daiquiri.core.utils import make_json_safe, worker
from daiquiri.core.responses import gzipped

logger = logging.getLogger(__name__)


ids = {
    "autoprocprogramid": fields.Int(description="Auto processing program id"),
    "autoprocprogramattachmentid": fields.Int(
        description="Auto processing program attachment id"
    ),
    "datacollectionid": fields.Int(description="Data collection id"),
    "type": fields.Str(enum=["processing"]),
}


class ContentsResource(ComponentResource):
    @marshal(
        inp=ids,
        out=[
            [200, RootHdf5Schema(), "Get hdf5 file contents"],
            [400, ErrorSchema(), "Could not find hdf5 file"],
        ],
    )
    def get(self, **kwargs):
        """Get the contents of an hdf5 file"""
        contents = self._parent.get_hdf5(**kwargs)
        if contents:

            def gzip():
                return gzipped(contents)

            return worker(gzip)

        return {"error": "Could not find hdf5 file"}, 400


class GroupResource(ComponentResource):
    @marshal(
        inp=ids,
        out=[
            [200, Hdf5Schema(), "Get hdf5 group contents, including data"],
            [400, ErrorSchema(), "Could not find hdf5 group"],
        ],
    )
    def get(self, path, **kwargs):
        """Get a group and its data from a hdf5 file"""
        group = self._parent.get_group(path, **kwargs)
        if group:

            def gzip():
                return gzipped(group)

            return worker(gzip)

        return {"error": "Could not find hdf5 group"}, 400


class Hdf5(Component):
    """Generic HDF5 Component

    A component that can read hdf5 files and return json slices
    of data. 

    Currently can get files from an
        autoprocprogramid (first rank file)
        autoprocprogramattachmentid 
        datacollectionid
    
    May have other sources in future
    """

    def setup(self, *args, **kwargs):
        self.register_route(ContentsResource, "")
        self.register_route(GroupResource, "/groups/<path:path>")

    def _file_from_app(self, autoprocprogramid):
        appas = self._metadata.get_autoprocprogram_attachments(
            autoprocprogramid=autoprocprogramid
        )

        rank = 9999
        minr = None
        for app in appas["rows"]:
            if app["rank"] < rank:
                ext = os.path.splitext(app["filename"])[1][1:].strip().lower()
                if (
                    app["rank"] is not None
                    and app["filetype"] == "Result"
                    and ext in ["h5", "hdf5", "nxs"]
                ):
                    rank = app["rank"]
                    minr = app

        if minr:
            return os.path.join(minr["filepath"], minr["filename"])

    def _get_file(self, **kwargs):
        """Find the file relevant for the request"""

        # From autoprocprogramid => lowest rank
        if kwargs.get("autoprocprogramid"):
            return self._file_from_app(kwargs["autoprocprogramid"])

        #  Directly from autoprocprogramattachmentid
        elif kwargs.get("autoprocprogramattachmentid"):
            appa = self._metadata.get_autoprocprogram_attachments(
                autoprocprogramattachmentid=kwargs["autoprocprogramattachmentid"]
            )
            print("appa", kwargs["autoprocprogramattachmentid"], appa)
            if appa:
                ext = os.path.splitext(appa["filename"])[1][1:].strip().lower()
                if appa["filetype"] == "Result" and ext in ["h5", "hdf5"]:
                    return appa["filefullpath"]

        # From datacollectionid, taking latest related autoprocprogram and lowest
        # rank attachment
        elif kwargs.get("datacollectionid") and kwargs.get("type") == "processing":
            apps = self._metadata.get_autoprocprograms(
                datacollectionid=kwargs.get("datacollectionid")
            )
            if apps["total"]:
                return self._file_from_app(apps["rows"][-1]["autoprocprogramid"])

        # Direct datacollection hdf5
        elif kwargs.get("datacollectionid"):
            dc = self._metadata.get_datacollections(
                datacollectionid=kwargs["datacollectionid"]
            )
            if dc:
                return os.path.join(dc["imagedirectory"], dc["filetemplate"])

    def _get_dataset(self, h5file, path, load_data=False):
        """Retrieve a dataset

        TODO: This will need sensible slicing options in the future

        Args:
            h5file (h5py.File): The h5 file instance
            path (str): uri to the dataset
            load_data (bool): Whether to load the data

        Returns:
            dataset (dict): Dict of the dataset
        """
        dataset = h5file[path]
        data = None
        if dataset.shape == ():
            data = dataset[()]
        else:
            if load_data:
                # TODO: Optimise for big datasets
                data = dataset[()]

        return {
            "type": "dataset",
            "data": data,
            "attrs": {attr: dataset.attrs[attr] for attr in dataset.attrs},
            "shape": dataset.shape,
            "size": dataset.size,
            "ndim": dataset.ndim,
            "dtype": dataset.dtype,
            "name": os.path.basename(dataset.name),
        }

    def _get_groups_and_attrs(self, h5file, path="/", load_data=False):
        """Retrieve a group

        Args:
            h5file (h5py.File): The h5 file instance
            path (str): uri to the dataset
            load_data (bool): Whether to load the data

        Returns:
            group (dict): Dict of the group
        """
        try:
            current = h5file[path]
        except KeyError:
            return

        children = {}
        for node in current:
            node_path = f"{path}/{node}"
            if isinstance(h5file[node_path], h5py.Dataset):
                child = self._get_dataset(h5file, node_path, load_data=load_data)
            else:
                child = self._get_groups_and_attrs(
                    h5file, node_path, load_data=load_data
                )

            children[child["name"]] = child

        return {
            "type": "group",
            "children": children,
            "name": os.path.basename(current.name),
            "uri": current.name,
            "attrs": {attr: current.attrs[attr] for attr in current.attrs},
        }

    def get_hdf5(self, **kwargs):
        file = self._get_file(**kwargs)
        if file:
            try:
                os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
                with h5py.File(file, mode="r") as h5file:
                    contents = self._get_groups_and_attrs(h5file)
                    if contents:
                        contents["file"] = os.path.basename(file)

                        return make_json_safe(contents)
            except OSError:
                logger.error(f"No such file {file}")

    def get_group(self, path, **kwargs):
        file = self._get_file(**kwargs)
        if file:
            try:
                with h5py.File(file, mode="r") as h5file:
                    group = self._get_groups_and_attrs(
                        h5file, path=path, load_data=True
                    )
                    if group:
                        return make_json_safe(group)

            except OSError:
                logger.error(f"No such file {file}")

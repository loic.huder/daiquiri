#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import logging

from marshmallow import Schema, fields
from flask import g

from daiquiri.core import marshal
from daiquiri.core.logging import log
from daiquiri.core.components import Component, ComponentResource
from daiquiri.core.schema import MessageSchema, ErrorSchema
from daiquiri.core.exceptions import PrettyException
from daiquiri.core.schema.validators import OneOf

logger = logging.getLogger(__name__)


class UIError(PrettyException):
    """A UI Error Exception (pretty)"""

    def __init__(self, message, frame=None, error=None):
        super().__init__(message)
        self.frame = frame
        self.error = error
        self.message = message

    def pretty(self):
        message = "A UI Error Occured\n"
        message += f"  {self.message}\n"
        if self.frame:
            message += f"    at {self.frame}\n"
        else:
            message += "    at [Could not resolve stacktrace]\n"
            message += f"      {self.error}\n"

        return message


class UIErrorSchema(Schema):
    status = OneOf(
        ["resolved", "error"],
        required=True,
        description="Whether the frame stacktrace was resolved",
    )
    message = fields.Str(required=True, description="The error messsage")
    frame = fields.Str(description="The frame stacktrace")
    error = fields.Str(
        description="The (secondary) error as to why the frame couldnt be resolved"
    )
    store = fields.Dict(description="The current store")


class LogSchema(Schema):
    logs = fields.Dict()


class LogResource(ComponentResource):
    @marshal(
        inp={"lines": fields.Int(), "offset": fields.Int()},
        out=[
            [200, LogSchema(), "Lines of log file"],
            [400, ErrorSchema(), "No access to log file"],
        ],
    )
    def get(self, **kwargs):
        """Get the last n lines of the user level log"""
        if g.user.staff():
            return {"logs": self._parent.get_logs(**kwargs)}
        else:
            return {"error": "No access"}, 400


class UILogResource(ComponentResource):
    @marshal(
        inp=UIErrorSchema,
        out=[
            [200, MessageSchema(), "Error logged successfully"],
            [400, ErrorSchema(), "Could not log error"],
        ],
    )
    def post(self, **kwargs):
        """Log a UI Error"""
        message = kwargs.pop("message")
        try:
            raise UIError(message, frame=kwargs.get("frame"), error=kwargs.get("error"))
        except UIError as e:
            log.get("ui").exception(message, type="ui", state=kwargs.get("store"))
            print(e.pretty())
            return {"message": "Error logged"}


class Logging(Component):
    """Logging Component

    Allows the application to retrieve historic logs from the json log file
    (to allow debuggin)
    """

    def setup(self, *args, **kwargs):
        self.register_route(LogResource, "")
        self.register_route(UILogResource, "/ui")

    def get_logs(self, **kwargs):
        lines = kwargs.get("lines", 50)
        offset = kwargs.get("offset", 0)

        out = {}
        for ty, file in log.list_files().items():
            with open(file, "rb") as f:
                out[ty] = [
                    json.loads(l.decode("utf-8"))
                    for l in self.tail(f, lines + offset)[::-1][offset:]
                ]

        return out

    def tail(self, f, window=1):
        """
        Returns the last `window` lines of file `f` as a list of bytes.
        https://stackoverflow.com/questions/136168/get-last-n-lines-of-a-file-with-python-similar-to-tail
        """
        if window == 0:
            return b""
        BUFSIZE = 1024
        f.seek(0, 2)
        end = f.tell()
        nlines = window + 1
        data = []
        while nlines > 0 and end > 0:
            i = max(0, end - BUFSIZE)
            nread = min(end, BUFSIZE)

            f.seek(i)
            chunk = f.read(nread)
            data.append(chunk)
            nlines -= chunk.count(b"\n")
            end -= nread
        return b"".join(reversed(data)).splitlines()[-window:]

#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
from functools import partial
import gevent
import time
import uuid
import json

from abc import ABC, abstractmethod
from marshmallow import Schema, fields, ValidationError

from daiquiri.core.exceptions import InvalidYAML
from daiquiri.core.logging import log
from daiquiri.core import (
    CoreBase,
    CoreResource,
    marshal,
    require_valid_session,
    require_staff,
)
from daiquiri.core.utils import loader
from daiquiri.resources.utils import get_resources, load_config, ConfigDict
from daiquiri.core.exceptions import SyntaxErrorYAML
from daiquiri.core.schema import MessageSchema, ErrorSchema
from daiquiri.core.schema.components import ComponentInfoSchema
from daiquiri.core.schema.metadata import paginated
from daiquiri.core.components.params import ParamSchema

import logging

logger = logging.getLogger(__name__)


def actor(name, **kwargs):
    def decorator(fn):
        fn.__actor_resource__ = {"name": name, "kwargs": kwargs}
        return fn

    return decorator


def actor_wrapper(name, **actkw):
    def decorator(fn):
        def wrapper(self, *args, **kwargs):
            if "preprocess" in actkw:
                #  Allow preprocessors to throw errors
                try:
                    preprocess = getattr(self, "preprocess")
                    kwargs = preprocess(*args, **kwargs)
                except Exception as e:
                    logger.exception("Could not create actor")
                    return {"error": f"Could not create actor: {str(e)}"}, 400

            actkw2 = {i: actkw[i] for i in actkw if i != "preprocess"}

            if hasattr(self._parent, "actor_success"):
                actkw2["success"] = self._parent.actor_success

            if hasattr(self._parent, "actor_started"):
                actkw2["start"] = self._parent.actor_started

            if hasattr(self._parent, "actor_error"):
                actkw2["error"] = self._parent.actor_error

            if hasattr(self._parent, "actor_remove"):
                actkw2["remove"] = self._parent.actor_remove

            uuid = self._parent.actor(name, actargs=kwargs, **actkw2)
            if not uuid:
                return {"error": "Could not load actor"}, 400

            ret = {}
            if not ("preprocess" in actkw):
                ret = kwargs

            log.get("user").info(f"New actor created '{name}'", type="actor")

            return dict({"uuid": uuid}, **ret), 200

        # TODO: hmm, need to dig into this, @wraps clobbers kwargs?
        if hasattr(fn, "_require_control"):
            wrapper._require_control = fn._require_control
        if hasattr(fn, "_require_staff"):
            wrapper._require_staff = fn._require_staff

        return wrapper

    return decorator


class AvailableComponentsResource(CoreResource):
    @marshal(out=[[200, paginated(ComponentInfoSchema), "List of components"]])
    def get(self):
        """Get a list of loaded components"""
        return self._parent.get_components(), 200


class ConfigExportResource(CoreResource):
    def get(self):
        """Returns any config that is exported from each of the loaded components"""
        return self._parent.get_export_config()


class ComponentReloadResource(CoreResource):
    @require_valid_session
    @require_staff
    @marshal(
        out=[
            [200, MessageSchema(), "Components reloaded"],
            [400, ErrorSchema(), "Could not reload components"],
        ]
    )
    def post(self):
        """Reload all components with new config"""
        if self._parent.reload():
            return {"message": "Components reloaded"}
        else:
            return {"error": "Could not reload components"}, 400


class Components(CoreBase):
    """Component Loader

    The core component class that dynamically loads components from components.yml
    Will try to load components from the core
    """

    _components = []

    def setup(self):
        self._config = []
        files = get_resources("config", "*.yml")
        for file in files:
            file_name = os.path.basename(file)
            config = load_config(file_name)
            if "component" in config:
                self._config.append({"type": config["component"], "config": file_name})

        for c in self._config + [
            {"type": "version"},
            {"type": "logging"},
            {"type": "chat"},
        ]:
            instance = loader(
                "daiquiri.core.components",
                "",
                c["type"],
                c.get("config"),
                **self.initkwargs,
            )

            if instance:
                instance.component_type = c["type"]
                instance.get_component = self.get_component
                self._components.append(instance)

        self.register_route(AvailableComponentsResource, "")
        self.register_route(ConfigExportResource, "/config")
        self.register_route(ComponentReloadResource, "/reload")

    def get_export_config(self):
        """Get exported config values from components"""
        export = {"beamline": self._base_config["meta_beamline"]}
        for c in self._components:
            export[c._base_url] = {}

            for k in c._config_export:
                export[c._base_url][k] = c._config[k]

        return export

    def reload(self):
        """Reload components

        Reload the root hardware object first, then reload listed components as
        these may depend on the hardware object

        Components must decide what to reload by implementing their own
        `reload` method
        """
        log.get("user").info("Reloading components", type="app")
        start = time.time()

        success = True
        try:
            self._hardware.reload()
        except (SyntaxErrorYAML, InvalidYAML) as ex:
            log.get("user").exception("Error in hardware config", type="hardware")
            print(ex.pretty())
            success = False
        else:
            for c in self._components:
                logger.info(f"Reloading config for {c.__class__.__name__}")
                try:
                    if isinstance(c._config, ConfigDict):
                        c._config.reload()
                        c.validate_config()

                except (SyntaxErrorYAML, InvalidYAML) as ex:
                    log.get("user").exception("Error in component config", type="app")
                    print(ex.pretty())
                    success = False
                else:
                    logger.info(f"Reloading component {c.__class__.__name__}")
                    c.reload()

            log.get("user").info(
                f"Components reloaded, took {(time.time()-start):.1f}s", type="app"
            )

            self.emit(
                "reloader", success, namespace="/app",
            )

            return True

    def get_component(self, type):
        for c in self._components:
            if c.component_type == type:
                return c

    def get_components(self):
        components = [f.info() for f in self._components]
        return {"total": len(components), "rows": components}


class ComponentResource(CoreResource):
    """ComponentResource that all component resources inherit from"""

    pass


class Component(CoreBase):
    """The abstract class that all components inherit from

    The component loads a config with the same name as the class.lower() and logs a 
    message if one cannot be found. The class also registeres a before_request handler
    to essentially enable middleware on the request, by default requiring a valid session
    and then checking require_control as needed

    The base component class also provide handling for execution and queuing of `Actors`
    """

    _config_schema = None
    _config_export = []

    _require_session = True
    _require_blsession = True

    _actors = []

    def __init__(self, *args, **kwargs):
        self._running_actors = {}

        if args[0]:
            self._config = load_config(args[0])
            self.validate_config()
        else:
            self._config = {}
        super().__init__(*args, **kwargs)

        logger.debug("Loading Component: {f}".format(f=self._bp))

    def after_setup(self):
        if self._namespace is None:
            logger.info(f"namespace is empty, defaulting to base_url: {self._base_url}")
            self._namespace = self._base_url
        self._session.register_namespace(self._namespace)

    def validate_config(self):
        if self._config_schema:
            try:
                self._config_schema.load(self._config)
            except ValidationError as err:
                raise InvalidYAML(
                    {
                        "message": f"{self.__class__.__name__} config is invalid",
                        "file": self._config.file,
                        "obj": self._config,
                        "errors": err.messages,
                    }
                ) from None

    def register_actor_route(self, route_class, route):
        for k in ["post", "get", "put", "patch", "delete"]:
            fn = getattr(route_class, k, None)
            if fn:
                if hasattr(fn, "__actor_resource__"):
                    actor = fn.__actor_resource__

                    fn = actor_wrapper(actor["name"], **actor["kwargs"])(fn)
                    sch = self.actor_schema(actor["name"])

                    if sch:
                        sch.reloader = partial(self.actor_schema, actor["name"])
                        if hasattr(fn, "_require_staff"):
                            sch._require_staff = fn._require_staff
                        fn = marshal(inp=sch)(fn)

                    setattr(route_class, k, fn)

        self.register_route(route_class, route)

    def info(self):
        """Return a dict of basic info about this component"""
        return {
            "name": self.__class__.__name__,
            "baseurl": "/" + self._bp.lower(),
            # 'config': self._config
        }

    def actor_schema(self, name):
        # The basemodule must be an importable python module
        basemodule = self._base_config["implementors"].replace("/", ".")
        actormodule = "{basemodule}.{cls}".format(
            basemodule=basemodule, cls=self.__class__.__name__.lower()
        )
        try:
            actor = loader(actormodule, "Actor", self._config["actors"][name])
        except:
            log.get("user").exception(
                f"Couldn't load actor {name} from {actormodule}", type="actor"
            )
            return

        if hasattr(actor, "schema"):
            return actor.schema
        else:
            logger.warning(f"Actor {name} does not have a schema")

    def actor(
        self,
        name,
        start=None,
        success=None,
        error=None,
        remove=None,
        enqueue=False,
        spawn=False,
        actargs={},
    ):
        """Launch / enqueue an actor

        Dynamically load an actor from config {implementors}/{class_name}/{file}
        File is determined from the component specific config file which maps name -> file

        Example:
        >>> config.yaml
        >>> implementors: implementors/examples

        >>> testcomponent.yml
        >>> actors:
        >>>   click: actor1
        >>>   scan: actor2

        >>> self.actor('click') will execute implementors/examples/testcomponent/actor1.py
        >>> self.actor('scan') will execute implementors/examples/testcomponent/actor2.py

        Args:
            name (str): The name of the actor to start, which is resolved from the config
            start (fn): Function to call when the actor starts
            success (fn): Function to call when the actor completes successfully
            error (fn): Function to call if the actor fails
            enqueue (boolean): Enqueue the actor rather than executing immediately
            actargs (dict): Dictionary of arguments to pass to the actor

        Returns:
            The actor uuid

        """
        if name in self._actors:
            actid = str(uuid.uuid4())
            # base must be an importable python module
            base = "{path}.{cls}".format(
                path=self._base_config["implementors"].replace("/", "."),
                cls=self.__class__.__name__.lower(),
            )
            basekw = {
                "uid": actid,
                "name": name,
                "metadata": self._metadata,
                "stomp": self._stomp,
                "started": self._actor_started,
                "finished": self._actor_finished,
                "error": self._actor_error,
                "_remove": self._actor_remove,
            }

            try:
                actor = loader(base, "Actor", self._config["actors"][name], **basekw)
            except:
                log.get("user").exception(
                    f"Couldn't load actor {name} from {base}", type="actor"
                )
                return

            if actor:
                self._running_actors[actid] = [actor, start, success, error, remove]

                actor.prepare(**actargs)
                if enqueue or actargs.get("enqueue"):
                    logger.debug(
                        "Enqueuing actor {act} with uid {uid}".format(
                            act=name, uid=actid
                        )
                    )
                    self._queue.push(actor)

                elif spawn:
                    logger.debug(
                        "Spawning actor {act} with uid {uid}".format(
                            act=name, uid=actid
                        )
                    )
                    gevent.spawn(actor.execute)

                else:
                    logger.debug(
                        "Running actor {act} with uid {uid}".format(act=name, uid=actid)
                    )
                    self._queue.run_now(actor)

                return actid

        else:
            logger.error(
                "No such actor {act} on class {cls}".format(act=name, cls=self._bp)
            )

    def _actor_started(self, actid):
        """Actor started callback

        Checks if the actorid is registered in the running actors, and then calls the started
        callback if registered

        Args:
            actid (uuid): The actor uuid that started
        """
        if not (actid in self._running_actors):
            logger.warning("Unknown actor started {uid}".format(uid=actid))
            return

        actor, start, success, error, remove = self._running_actors[actid]
        logger.debug(
            "Actor started {name} {actid}".format(name=actor.name, actid=actid)
        )

        if start:
            start(actid, actor)

    def _actor_finished(self, actid):
        """Actor started callback

        Checks if the actorid is registered in the running actors, and then calls the finished
        callback if registered

        Args:
            actid (uuid): The actor uuid that finished
        """
        if not (actid in self._running_actors):
            logger.warning("Unknown actor completed {uid}".format(uid=actid))
            return

        actor, start, success, error, remove = self._running_actors[actid]
        logger.debug(
            "Actor finished {name} {actid} took {s}".format(
                name=actor.name, actid=actid, s=actor.took()
            )
        )

        if success:
            success(actid, actor.resp(), actor)

        del self._running_actors[actid]

    def _actor_error(self, actid, exception):
        """Actor error callback

        Checks if the actorid is registered in the running actors, and then calls the error
        callback if registered

        Args:
            actid (uuid): The actor uuid that failed
        """
        if not (actid in self._running_actors):
            logger.warning("Unknown actor error {uid}".format(uid=actid))
            return

        actor, start, success, error, remove = self._running_actors[actid]
        log.get("user").exception(
            f"Actor failed {actor.name}: {exception}", type="actor"
        )

        try:
            eactid = str(uuid.uuid4())
            eactor = loader(
                self._base_config["implementors"].replace("/", "."),
                "Actor",
                "upload_error",
                **{
                    "uid": eactid,
                    "metadata": self._metadata,
                    "started": lambda *args, **kwargs: None,
                    "finished": lambda *args, **kwargs: None,
                    "error": lambda *args, **kwargs: None,
                },
            )
            eactor.prepare(**{"actid": actid, "exception": exception, "actor": actor})
            gevent.spawn(eactor.execute)

        except Exception:
            logger.exception("Could not send actor error to uploader")

        if error:
            error(actid, exception, actor)

        del self._running_actors[actid]

    def _actor_remove(self, actid):
        """Actor remove callback
        
        Called if an actor is removed from the queue

        Args:
            actid (uuid): The actor uuid that has been removed

        """
        if not (actid in self._running_actors):
            logger.warning("Unknown actor error {uid}".format(uid=actid))
            return

        actor, start, success, error, remove = self._running_actors[actid]
        logger.debug(
            "Actor removed {name} {actid}".format(name=actor.name, actid=actid)
        )

        if remove:
            remove(actid, actor)


class ComponentActor(ABC):
    """Actor

    The abstract actor from which all actors inherit

    """

    name = None
    desc = None
    metatype = "experiment"
    saving_args = None

    def __init__(self, *args, **kwargs):
        self._running = False
        self.__dict__.update((k, v) for k, v in kwargs.items())

    def time_estimate(self):
        if hasattr(self, "schema"):
            if hasattr(self.schema, "time_estimate"):
                return self.schema().time_estimate(self.initkwargs)
        return 0

    def prepare(self, **kwargs):
        self.initkwargs = kwargs
        self._estimate = self.time_estimate()
        self._start = None
        self._finished = None
        self._failed = False
        self.created = time.time()
        self.data = {}

    def __getitem__(self, key):
        """Get value from initial arguments or data"""
        try:
            return self.initkwargs[key]
        except KeyError:
            pass
        return self.data[key]

    def get(self, key, default=None):
        """Get value from initial arguments or data"""
        try:
            return self[key]
        except KeyError:
            return default

    def __setitem__(self, key, value):
        """Overwrite data"""
        self.data[key] = value

    def update(self, *args, **kwargs):
        """Overwrite data"""
        self.data.update(*args, kwargs)

    @property
    def all_data(self):
        """Initial arguments + data"""
        return {**self.data, **self.initkwargs}

    @property
    def initkwargs_json_serializable(self):
        """Actor arguments prepared for json serialization"""
        return self._make_json_serializable(self.initkwargs)

    @property
    def initkwargs_json_serialized(self):
        """Json serialized actor arguments"""
        return json.dumps(self.initkwargs_json_serializable)

    @property
    def all_data_json_serializable(self):
        """Actor arguments + data prepared for json serialization"""
        return self._make_json_serializable(self.all_data)

    @property
    def all_data_json_serialized(self):
        """Json serialized actor arguments + data"""
        return json.dumps(self.all_data_json_serializable)

    @property
    def data_json_serializable(self):
        """Actor data prepared for json serialization"""
        return self._make_json_serializable(self.data)

    @property
    def data_json_serialized(self):
        """Json serialized actor data"""
        return json.dumps(self.data_json_serializable)

    def _make_json_serializable(self, adict):
        """Make a dictionary json serializable

        :param dict adict:
        :returns dict:
        """
        safe_dict = {}
        for k, v in adict.items():
            if isinstance(v, dict):
                safe_dict[k] = self._make_json_serializable(v)
            else:
                try:
                    json.dumps(v)
                except TypeError:
                    safe_dict[k] = str(v)
                else:
                    safe_dict[k] = v
        return safe_dict

    def handle_params(self, before=True):
        """ Handle special `ParamSchema` params

        If a schema contains an instance of `ParamSchema` process these before
        and after an actor. 
        """
        if hasattr(self, "schema"):
            schema = self.schema()
            for k, v in schema.fields.items():
                if isinstance(v, fields.Nested):
                    if issubclass(v.nested, ParamSchema):
                        if k in self.initkwargs:
                            handler = v.nested.handler
                            for p, pv in self.initkwargs[k].items():
                                ty = "before" if before else "after"
                                par = {}
                                par[p] = pv
                                try:
                                    log.get("user").info(
                                        f"Running {k}.{p} to {pv} {ty} actor",
                                        type="hardware",
                                    )
                                    handler.handle(par, before)
                                    log.get("user").info(
                                        f"Finished {k}.{p} {ty} actor", type="hardware"
                                    )
                                except Exception as e:
                                    logger.exception(
                                        f"Cannot handle {k}.{p} to {pv} {ty} actor, {str(e)}"
                                    )
                                    log.get("user").exception(
                                        f"Cannot handle {k}.{p} to {pv} {ty} actor, {str(e)}",
                                        type="hardware",
                                    )

    def execute(self):
        """Execute the actor

        Try to execute the actor, time it, and catch any errors it may raise
        """
        self._running = True
        self._start = time.time()
        try:
            self.started(self.uid)
            self.handle_params()
            self._resp = self.method(
                **self.initkwargs
            )  # TODO: Why do we need to pass these arguments
            self._finished = time.time()
            self._running = False
            self.handle_params(before=False)
            self.finished(self.uid)

        # To catch gevent.Timeout as well
        except BaseException as e:
            self._running = False
            self._failed = True
            self._finished = time.time()
            self.handle_params(before=False)
            self.error(self.uid, e)
            return e

    def info(self):
        """Actor info

        Returns
            A dict of the actor info
        """
        return {
            "created": self.created,
            "started": self._start,
            "estimate": self._estimate,
            "finished": self._finished,
            "failed": self._failed,
            "name": self.name,
            "desc": self.desc,
            "uid": self.uid,
            "running": self._running,
            "cls": repr(self),
            "args": self.initkwargs_json_serializable,
        }

    def running(self):
        """The running state of the actor"""
        return self._running

    def resp(self):
        """The response from the finished actor"""
        return self._resp

    def took(self):
        """How long the actor took"""
        return self._finished - self._start

    @abstractmethod
    def method(self, *args, **kwargs):
        """The actual actor definition"""
        pass

    def remove(self):
        """Method is called if an actor is remove from the queue"""
        if hasattr(self, "_remove"):
            self._remove(self.uid)


class ComponentActorKilled(Exception):
    """Exception to raise when an actor is killed"""

    pass


class ComponentActorSchema(Schema):
    """Component Actor Schema

    Marks a schema for caching (meta)
    Allows schema reloading if `reloader` is set
    """

    reloader = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.Meta.cache = True

    def reload_schema(self):
        if self.reloader:
            schema = self.reloader()
            instance = schema()

            for key in [
                "fields",
                "_declared_fields",
                "warnings",
                "calculated",
                "time_estimate",
                "schema_validate",
                "save_preset",
                "get_presets",
            ]:
                if hasattr(instance, key):
                    setattr(self, key, getattr(instance, key))

            if schema.Meta:
                for key in ["uiorder", "uischema", "uigroups", "presets", "cache"]:
                    if hasattr(schema.Meta, key):
                        setattr(self.Meta, key, getattr(schema.Meta, key))

    def __getattribute__(self, name):
        if name == "fields" or name == "_declared_fields":
            self.reload_schema()

        return super().__getattribute__(name)

from marshmallow import Schema, fields


class LayoutSchema(Schema):
    name = fields.Str(required=True, description="Layout name")
    description = fields.Str(description="Description of layout")
    acronym = fields.Str(description="Layout acronym, used in url slug")
    contents = fields.List(fields.Dict(), required=True)

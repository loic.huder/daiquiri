#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty, OneOf

import logging

logger = logging.getLogger(__name__)

BeamviewerStates = ["ON", "OFF", "UNKNOWN", "FAULT"]


class BeamviewerPropertiesSchema(HardwareSchema):
    state = OneOf(BeamviewerStates, readOnly=True)
    foil = OneOf(["IN", "OUT", "UNKNOWN", "NONE"], readOnly=True)
    led = OneOf(["ON", "OFF"], readOnly=True)
    screen = OneOf(["IN", "OUT", "UNKNOWN"], readOnly=True)
    diode_ranges = fields.List(fields.Str(), readOnly=True)
    diode_range = fields.Str()


class BeamviewerCallablesSchema(HardwareSchema):
    led = fields.Bool()
    screen = fields.Bool()
    foil = fields.Bool()
    current = RequireEmpty()


class Beamviewer(HardwareObject):
    _type = "beamviewer"
    _state_ok = [BeamviewerStates[0], BeamviewerStates[1]]

    _properties = BeamviewerPropertiesSchema()
    _callables = BeamviewerCallablesSchema()

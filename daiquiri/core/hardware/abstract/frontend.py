#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty, OneOf

import logging

logger = logging.getLogger(__name__)

FrontendStates = ["OPEN", "RUNNING", "STANDBY", "CLOSED", "UNKNOWN", "FAULT"]
FrontendItlkStates = ["ON", "FAULT"]


class FrontendPropertiesSchema(HardwareSchema):
    state = OneOf(FrontendStates, readOnly=True)
    status = fields.Str(readOnly=True)

    automatic = fields.Bool(readOnly=True)
    frontend = fields.Str(readOnly=True)

    current = fields.Float(readOnly=True)
    refill = fields.Float(readOnly=True)
    mode = fields.Str(readOnly=True)
    message = fields.Str(readOnly=True)

    feitlk = OneOf(FrontendItlkStates, readOnly=True)
    pssitlk = OneOf(FrontendItlkStates, readOnly=True)
    expitlk = OneOf(FrontendItlkStates, readOnly=True)


class FrontendCallablesSchema(HardwareSchema):
    open = RequireEmpty()
    close = RequireEmpty()
    reset = RequireEmpty()


class Frontend(HardwareObject):
    _type = "frontend"
    _state_ok = [FrontendStates[0], FrontendStates[1]]

    _properties = FrontendPropertiesSchema()
    _callables = FrontendCallablesSchema()

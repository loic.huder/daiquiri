#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty, OneOf

import logging

logger = logging.getLogger(__name__)

ShutterStates = ["OPEN", "CLOSED", "FAULT", "UNKNOWN"]


class ShutterPropertiesSchema(HardwareSchema):
    state = OneOf(ShutterStates, readOnly=True)
    status = fields.Str(readOnly=True)
    valid = fields.Bool()
    open_text = fields.Str(readOnly=True)
    closed_text = fields.Str(readOnly=True)


class ShutterCallablesSchema(HardwareSchema):
    open = RequireEmpty()
    close = RequireEmpty()
    toggle = RequireEmpty()
    reset = RequireEmpty()


class Shutter(HardwareObject):
    _type = "shutter"
    _state_ok = [ShutterStates[0], ShutterStates[1]]

    _properties = ShutterPropertiesSchema()
    _callables = ShutterCallablesSchema()

    def _call_toggle(self):
        if self.get("state") == "OPEN":
            self.call("close", None)
        else:
            self.call("open", None)

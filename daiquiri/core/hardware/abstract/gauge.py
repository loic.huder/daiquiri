#!/usr/bin/env python
# -*- coding: utf-8 -*-
from marshmallow import Schema, fields

from daiquiri.core.hardware.abstract import HardwareObject
from daiquiri.core.schema.hardware import HardwareSchema
from daiquiri.core.schema.validators import RequireEmpty, OneOf

import logging

logger = logging.getLogger(__name__)

GaugeStates = ["ON", "OFF", "UNKNOWN", "ERROR"]


class GaugePropertiesSchema(HardwareSchema):
    state = OneOf(GaugeStates, readOnly=True)
    status = fields.Str(readOnly=True)
    pressure = fields.Float(readOnly=True)


class GaugeCallablesSchema(HardwareSchema):
    on = RequireEmpty()
    off = RequireEmpty()


class Gauge(HardwareObject):
    _type = "gauge"
    _state_ok = [GaugeStates[0], GaugeStates[1]]

    _properties = GaugePropertiesSchema()
    _callables = GaugeCallablesSchema()

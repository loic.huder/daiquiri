#!/usr/bin/env python
# -*- coding: utf-8 -*-
import importlib
import logging

from marshmallow import ValidationError, fields
from bliss.config.static import get_config

from daiquiri.core.hardware.abstract import ProtocolHandler
from daiquiri.core.hardware.bliss.object import BlissDummyObject
from daiquiri.core.schema.hardware import HOConfigSchema
from daiquiri.core.exceptions import InvalidYAML
from daiquiri.core.utils import loader


logger = logging.getLogger(__name__)

bl = logging.getLogger("bliss")
bl.disabled = True

bl = logging.getLogger("bliss.common.mapping")
bl.disabled = True


class BlissHOConfigSchema(HOConfigSchema):
    """The Bliss Hardware Object Config Schema"""

    address = fields.Str(required=True, description="Beacon object id")


class BlissHandler(ProtocolHandler):
    """The bliss protocol handler

    Returns an instance of an abstracted bliss object

    The bliss protocol handler first checks the kwargs conform to the BlissHOConfigSchema
    defined above. This address is used to retrieve the  bliss object. Its class is then mapped 
    to an abstract class and a bliss specific instance is created (see hardware/bliss/motor.py)
    """

    _class_map = {
        "bliss.common.axis.Axis": "motor",
        "bliss.controllers.actuator.Actuator": "actuator",
        "bliss.controllers.multiplepositions.MultiplePositions": "multiposition",
        "bliss.common.shutter.Shutter": "shutter",
    }

    _class_name_map = {
        "EBV": "beamviewer",
        "ChannelFromConfig": "channelfromconfig",
        "volpi": "volpi",
    }

    def get(self, **kwargs):
        try:
            BlissHOConfigSchema().load(kwargs)
        except ValidationError as err:
            raise InvalidYAML(
                {
                    "message": "Bliss hardware object definition is invalid",
                    "file": "hardware.yml",
                    "obj": kwargs,
                    "errors": err.messages,
                }
            ) from err

        config = get_config()
        try:
            kwargs["obj"] = config.get(kwargs.get("address"))
        except Exception:
            logger.error(f"Couldnt get bliss object {kwargs.get('address')}")
            return BlissDummyObject(**kwargs)

        else:
            for bliss_mapping, mapped_class in self._class_map.items():
                bliss_file, bliss_class_name = bliss_mapping.rsplit(".", 1)
                bliss_module = importlib.import_module(bliss_file)
                bliss_class = getattr(bliss_module, bliss_class_name)
                if isinstance(kwargs["obj"], bliss_class):
                    return loader(
                        "daiquiri.core.hardware.bliss", "", mapped_class, **kwargs,
                    )

            cls = kwargs["obj"].__class__.__name__
            if cls in self._class_name_map:
                return loader(
                    "daiquiri.core.hardware.bliss",
                    "",
                    self._class_name_map[cls],
                    **kwargs,
                )

            raise Exception("No class found for {cls}".format(cls=cls))

#!/usr/bin/env python
# -*- coding: utf-8 -*-
from daiquiri.core.hardware.abstract import HardwareTranslator
from daiquiri.core.hardware.abstract.shutter import Shutter as AbstractShutter
from daiquiri.core.hardware.bliss.object import BlissObject

import logging

logger = logging.getLogger(__name__)


class ShutterTranslator(HardwareTranslator):
    def from_state(self, value):
        return self._parent._object.state_string.upper()


class Shutter(BlissObject, AbstractShutter):
    translator = ShutterTranslator

    property_map = {
        # TODO:ideally this would have a channel
        #   Now has a channel, but doesnt work?
        "state": "state",
    }

    callable_map = {"open": "open", "close": "close", "toggle": "toggle"}

    def _get_status(self):
        if hasattr(self._object, "_tango_status"):
            return self._object._tango_status
        else:
            return ""

    def _get_open_text(self):
        return ""

    def _get_closed_text(self):
        return ""

    def _get_valid(self):
        return True

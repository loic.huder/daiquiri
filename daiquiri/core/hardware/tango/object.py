#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import time
import gevent

import tango
from tango.gevent import DeviceProxy

from daiquiri.core.hardware.abstract import MappedHardwareObject

import logging

logger = logging.getLogger(__name__)


class TangoObject(MappedHardwareObject):
    _protocol = "tango"

    def __init__(self, **kwargs):
        self._subscriptions = {}
        self._tango_url = kwargs["tango_url"]
        self._object = DeviceProxy(self._tango_url)
        super().__init__(**kwargs)
        gevent.spawn(self.try_device)
        # TODO: Temporary fix for
        # https://github.com/tango-controls/TangoTickets/issues/34
        time.sleep(1)

    def try_device(self):
        while True:
            while not self._online:
                try:
                    logger.info(f"Trying to connect to {self._tango_url}")
                    self._object.ping()
                    self.subscribe_device()
                    self.set_online(True)
                    self.broadcast()
                except tango.ConnectionFailed as e:
                    logger.error(
                        f"Could not connect to {self._tango_url}. Retrying in 10s"
                    )
                except tango.DevFailed as e:
                    logger.error(
                        f"Could not connect to {self._tango_url}. Retrying in 10s"
                    )
                except Exception as e:
                    logger.exception(
                        f"Could not connect to {self._tango_url}. Retrying in 10s"
                    )

                gevent.sleep(10)

            gevent.sleep(10)

    def broadcast(self):
        for p in self.property_map:
            self._update(p, self._get(p))

    def _subscribe(self, p):
        self._subscriptions[p] = self._object.subscribe_event(
            p,
            tango.EventType.CHANGE_EVENT,
            self.push_event,
            green_mode=tango.GreenMode.Gevent,
        )

    def subscribe_device(self):
        for p in self.property_map.values():
            try:
                self._subscribe(p)

            except tango.DevFailed as e:
                logger.info(f"Starting polling for {self._tango_url} {p}")
                if e.args[0].reason == "API_AttributePollingNotStarted":
                    try:
                        self._object.poll_attribute(p, 1000)
                        self._subscribe(p)
                    except Exception as e:
                        logger.exception(f"Could not poll attribute {p}")

            except Exception as e:
                logger.exception(f"Could not subscribe to property {p}")

    def unsubscribe_device(self):
        for p, eid in self._subscriptions.items():
            try:
                self._object.unsubscribe_event(eid)
            except Exception as e:
                logger.error(f"Couldnt unsubscribe from {p}")

        self._subscriptions = {}

    def push_event(self, event):
        if event.errors and self._online:
            error = event.errors[0]
            logger.error(f"Error in push_event for {event.attr_name}: {error.desc}")
            # if error.reason == 'API_EventTimeout':
            self.unsubscribe_device()
            self.set_online(False)

        if not self._online:
            return

        if event.attr_value is not None:
            for p, v in self.property_map.items():
                if event.attr_value.name == v:
                    self._update(p, event.attr_value.value)

    def _do_set(self, prop, value):
        return self._object.write_attribute(self.property_map[prop], value)

    def _do_get(self, prop):
        return self._object.read_attribute(self.property_map[prop]).value

    def _do_call(self, function, value, **kwargs):
        return self._object.command_inout(function, value, **kwargs)

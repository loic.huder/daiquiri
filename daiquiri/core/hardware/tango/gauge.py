#!/usr/bin/env python
# -*- coding: utf-8 -*-
from tango import DevState

from daiquiri.core.hardware.abstract import HardwareTranslator
from daiquiri.core.hardware.abstract.gauge import Gauge as AbstractGauge
from daiquiri.core.hardware.tango.object import TangoObject

import logging

logger = logging.getLogger(__name__)


class GaugeTranslator(HardwareTranslator):
    def from_state(self, value):
        val_map = {DevState.ON: "ON", DevState.OFF: "OFF", DevState.UNKNOWN: "UNKNOWN"}
        for k, v in val_map.items():
            if k == value:
                return v

        return "UNKNOWN"


class Gauge(TangoObject, AbstractGauge):
    translator = GaugeTranslator

    property_map = {"state": "state", "status": "status", "pressure": "pressure"}

    callable_map = {"on": "on", "off": "off"}

#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import json
import logging
import uuid
from pprint import pformat

import gevent
from stompest.config import StompConfig
from stompest.protocol import StompSpec
from stompest.sync import Stomp as StompClient
from stompest.error import StompConnectionError, StompConnectTimeout, StompProtocolError

from daiquiri.core import CoreBase
from daiquiri.core.utils import make_json_safe


logger = logging.getLogger(__name__)


def format_body(body):
    """Parse zocalo recipe body"""
    message = {}

    if not isinstance(body, dict):
        logger.warning("Message body is empty")
        return message

    try:
        current_step = body["recipe"][str(body["recipe-pointer"])]
    except KeyError:
        logger.warning("Could not find recipe step")
        return message
    else:
        message.update(current_step["parameters"])
        message.update(body["payload"])
        environment = body["environment"]

        for key in sorted(environment, key=len, reverse=True):
            for mk, value in message.items():
                if isinstance(value, str):
                    if "$" + key == value:
                        message[mk] = value.replace("$" + key, str(environment[key]))

    return message


class Stomp(CoreBase):
    """Core Stomp Interface

    Initialise stomp to connect to the message broker. If a local queue for this 
    instance of daiquiri has been enabled subscribe to the queue. Otherwise just
    allow daiquiri to be able to send messages
    """

    _namespace = "stomp"
    _client = None
    _running = False
    _last_heart_beat = 0
    _callbacks = []
    _backoff = 0

    _last_emit_time = 0
    _emit_timeout = None
    _messages_received = {}

    def read_client(self):
        """Read queue

        Read from queue and parse incoming messages
        """
        while self._running:
            try:
                if self._client.canRead(1):
                    frame = self._client.receiveFrame()
                    self._client.ack(frame)
                    if frame.command == "MESSAGE":
                        message = format_body(json.loads(frame.body))
                        self.on_message_recieved(frame.headers, message)

                now = time.time()
                if now - self._last_heart_beat > 30:
                    self._client.beat()
                    self._last_heart_beat = now

            except StompConnectionError:
                logger.warning("Could not connect to broker")
                try:
                    self.connect_and_subscribe()
                except StompConnectTimeout:
                    logger.warning("Timeout connecting to broker")
                    self._backoff += 1

            gevent.sleep(0.1)

            if self._backoff > 0:
                logger.warning("Waiting for backoff")
                gevent.sleep(self._backoff * 10)
                self._backoff -= 1

    def connect_and_subscribe(self):
        logger.info("Trying to connect to stomp")
        prefix = self._config.get("stomp_queue_prefix", "zocalo")
        queue = f"{prefix}.transient.{self._config['meta_beamline']}.notification"
        logger.info(f"Subscribing to queue: {queue}")

        self._client.connect()
        try:
            self._client.subscribe(
                f"/queue/{queue}",
                {
                    StompSpec.ACK_HEADER: StompSpec.ACK_CLIENT_INDIVIDUAL,
                    StompSpec.ID_HEADER: f"daiquiri_{self._config['meta_beamline']}",
                },
            )
        except StompProtocolError as e:
            logger.warning(f"StompProtocolError: {str(e)}")

        logger.info("Connected to broker and subscribed to queue")

    def setup(self):
        self._stomp_config = StompConfig(
            f"tcp://{self._config['stomp_host']}:{self._config['stomp_port']}",
            version=StompSpec.VERSION_1_1,
        )

        if self._config.get("stomp_queue"):
            self._client = StompClient(self._stomp_config)
            self._running = True
            self._read_thread = gevent.spawn(self.read_client)

    def __del__(self):
        if self._running:
            self._running = False
            self._read_thread.join()

        if self._client:
            self._client.disconnect()

    def add_listener(self, callback):
        """Add a stomp listener
        
        Args:
            callback(function):  Callback should be of the form callback(headers, message)
        """
        if not callable(callback):
            raise AttributeError(f"Callback is not callable: {callback}")

        if callback not in self._callbacks:
            self._callbacks.append(callback)
        else:
            raise AttributeError(f"Callback is already registered: {callback}")

    def on_message_recieved(self, headers, message):
        """Incoming message handler

        Args:
            headers (dict): Message header
            message (dict): Message body
        """
        logger.debug("Message recieved from stomp: %s", pformat(message))
        for callback in self._callbacks:
            callback(headers, message)

        if message.get("daiquiri_id"):
            self._messages_received[message.get("daiquiri_id")] = {
                "time": time.time(),
                "message": message,
            }

        self._queue_emit_message(headers, message)

    def _queue_emit_message(self, headers, message):
        """Debounce event emission

        Try to not spam client
        """
        if self._emit_timeout is not None:
            self._emit_timeout.kill()
            self._emit_timeout = None

        now = time.time()
        if now - self._last_emit_time > 0.2:
            self._emit_message(headers, message)
        else:
            self._emit_timeout = gevent.spawn_later(
                0.2, self._emit_message, headers, message
            )

    def _emit_message(self, headers, message):
        self.emit("message", message)
        self._last_emit_time = time.time()

    def send_message(self, message, destination=None):
        """Send a message to a queue

        Args:
            message (dict): Dictionary of the message parameters
            destination (str): The queue to send the message to
        """
        if destination is None:
            destination = self._config["stomp_destination"]

        if not message["parameters"]:
            message["parameters"] = {}
        message["parameters"]["daiquiri_id"] = str(uuid.uuid4())

        try:
            safe_message = make_json_safe(message)
            client = StompClient(self._stomp_config)
            client.connect()
            client.send(
                f"/queue/{destination}", json.dumps(safe_message).encode("utf-8")
            )
            client.disconnect()
            return message["parameters"]["daiquiri_id"]
        except:
            logger.exception("Problem sending message to broker, message not sent")

    def start_recipe(self, datacollectionid, recipe, parameters):
        """Start a processing recipe

        Helper function to launch a processing recipe
        """
        parameters.update({"ispyb_dcid": datacollectionid})
        uuid = self.send_message({"recipes": [recipe], "parameters": parameters})
        self.emit(
            "message",
            {
                "type": "start_recipe",
                "recipe": recipe,
                "datacollectionid": datacollectionid,
                "daiquiri_id": uuid,
            },
        )
        return uuid

    def wait_for(self, uuid, timeout=60 * 5):
        start = time.time()
        while not self._messages_received.get(uuid):
            time.sleep(1)

            if time.time() - start > timeout:
                logger.warning(
                    f"Did not get response for uuid: {uuid} after timeout {timeout}s"
                )
                return

        return self._messages_received[uuid].get("message")

    def send_event(self, datacollectionid, event):
        """Send a processing event

        Helper function to send an event message
        """
        if event not in ["start", "end"]:
            raise AttributeError(f"Unknown event {event}")

        uuid = self.send_message(
            {
                "recipes": ["mimas"],
                "parameters": {"ispyb_dcid": datacollectionid, "event": event},
            }
        )

        self.emit(
            "message",
            {
                "type": "event",
                "event": event,
                "datacollectionid": datacollectionid,
                "daiquiri_id": uuid,
            },
        )

        return uuid

"""All non-python files used by the project.

When `daiquiri` is looking for a resource, it first tries looking in
the resource directory passed as an argument at startup  (see app.py) 
and then in this module.
"""

import os
from glob import glob
import logging
import yaml
from daiquiri.core.exceptions import SyntaxErrorYAML


logger = logging.getLogger(__name__)

RESOURCE_ROOTS = [os.path.join(os.path.dirname(__file__))]


class IncludeTag(yaml.YAMLObject):
    """Add an !include yaml tag

    Allows referencing yaml files from definitions
    Usage:
        my_value: !include extra.yml     # whole file
        my_value: !include extra.yml#key # just key (assuming obj is dict)
    """

    yaml_tag = "!include"

    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return "IncludeTag({self.value})"

    @classmethod
    def from_yaml(cls, loader, node):
        parts = node.value.split("#")
        ref = parts[1] if len(parts) > 1 else None
        root = loader.stream.name
        reference = f"in !include '{root}', line {node.start_mark.line}, column {node.start_mark.column}"

        filename = os.path.abspath(
            os.path.join(os.path.dirname(loader.stream.name), parts[0])
        )
        extension = os.path.splitext(filename)[1][1:]
        if extension not in ("yaml", "yml"):
            raise SyntaxErrorYAML(
                {
                    "error": f"!include file must have extension 'yml' or 'yaml'\n  extension was {extension}\n{reference}"
                }
            )

        with open(filename, "r") as f:
            try:
                contents = yaml.safe_load(f)
            except yaml.YAMLError as ex:
                print("yaml error")
                raise SyntaxErrorYAML({"error": ex}) from None

            else:
                if ref is not None:
                    if not isinstance(contents, dict):
                        raise SyntaxErrorYAML(
                            {
                                "error": f"!include has #{ref}, but contents of {filename} is not an object\n  type was {type(contents)}\n{reference}"
                            }
                        )

                    try:
                        return contents[ref]
                    except KeyError:
                        raise SyntaxErrorYAML(
                            {
                                "error": f"no such key '{ref}' in '{filename}'\n{reference}"
                            }
                        )

                else:
                    return contents

    @classmethod
    def to_yaml(cls, dumper, data):
        return dumper.represent_scalar(cls.yaml_tag, data.value)


yaml.SafeLoader.add_constructor("!include", IncludeTag.from_yaml)


def add_resource_root(path):
    """The new resource root directory will be used
    before the existing ones
    """
    if path in RESOURCE_ROOTS:
        RESOURCE_ROOTS.pop(RESOURCE_ROOTS.index(path))
    RESOURCE_ROOTS.insert(0, path)


def get_resource_path(root, resourcetype, resourcename):
    """Get the resource directory

    :param str root: one of the resource roots
    :param str resourcetype: subdirectory
    :param str resourcename: filename
    :returns str:
    """
    return os.path.join(root, *resourcetype.split("."), resourcename)


def get_resource(resourcetype, resourcename):
    """Get resource from the resource directory specified
    by the REST app CLI. Try local resource directory
    when missing.

    :param str resourcetype: subdirectory
    :param str resourcename: filename
    :raises RuntimeError: resource does not exist
    :returns str:
    """
    for root in RESOURCE_ROOTS:
        fpath = get_resource_path(root, resourcetype, resourcename)
        if os.path.exists(fpath):
            break
    else:
        msg = f"No such resource {resourcetype}.{resourcename}"
        logger.warning(msg)
        raise RuntimeError(msg)
    logger.info(f"Using resource '{fpath}'")
    return fpath


def get_resources(resourcetype, pattern="*"):
    """Get resources from the resource directory specified
    by the REST app CLI. Try local resource directory
    when missing.

    :param str resourcetype: subdirectory
    :param str resourcename: filename pattern
    :returns list(str):
    """
    for root in RESOURCE_ROOTS:
        files = glob(get_resource_path(root, resourcetype, pattern))
        if files:
            logger.info(f"Using resources {files}")
            return files
    return []


class ConfigDict(dict):
    file = None
    path = None

    def reload(self):
        logger.info(f"Reloading {self.file}")
        with open(self.path, "r") as stream:
            try:
                self.clear()
                self.update(yaml.safe_load(stream))
            except yaml.YAMLError as ex:
                raise SyntaxErrorYAML({"error": ex}) from None


def load_config(fname):
    """Load config resource as a dictionary

    :returns ConfigDict:
    """
    fpath = get_resource(resourcetype="config", resourcename=fname)
    with open(fpath, "r") as stream:
        try:
            config = ConfigDict(yaml.safe_load(stream))
        except yaml.YAMLError as ex:
            raise SyntaxErrorYAML({"error": ex}) from None
    config.file = fname
    config.path = fpath
    return config

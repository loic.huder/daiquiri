The imageviewer component allows the server to interact with a 2d camera and a series of associated sample stages. It can move marked positions (`subsamples`) to the beam and execute [Actors](actors.md) against a series of coordinates. It can also capture images from the image source and initiate post processing

# Configuration
## Actors
The actors configuration section maps the required actors to their corresponding files, key value pairs can be left the same for standard mapping
```yaml
actors:
  createmap: createmap
  move: move
  mosaic: mosaic
  roiscan: roiscan
  poiscan: poiscan
  loiscan: loiscan
```

## Misc options
```yaml
# Types of actors from which to launch auto map generation
createmaps:
  - roiscan

# Time in seconds between checking actors
regenerate_interval: 60

# File saving for sample images. 
# For esrf saving policy this should define `dataset` instead
sample_image_saving:
  # Simple saving
  data_filename: "{sampleid.name}_image_{time}"

  # ESRF saving
  dataset: "{sampleid.name}_image_{time}"
```

## Sources
Sources define the video source and associated motors for the imageviewer
```yaml
sources:
  - name: vlm
    # The lima device providing video
    device: lima_simulator

    # Url to the video streaming device
    url: https://123.123.123.123:5000/video

    # The hardware object for the zoom
    zoom: beamstop

    # Pixel sizes and focal point position for each zoom
    zoominfo:
      # The key is a the multiposition value
      IN:
        # Negative pixel size denotes an image flip
        # The imageviewer expects the image to be defined as bottom left corner as (0,0)
        pixelsize: [-1639.345, -1637.431]

        # The focal point is defined in pixels from the centre of the image
        focalpoint: [-20, 40]

      OUT:
        pixelsize: [-5633.802, -5133.802]
        focalpoint: [-30, 50]

    # Motors are defined in normal x,y,z coordinate system 
    #   (not beamline coordinate system(!))

    # Motor for x translation
    motor_x: m1
    # If units are not defined on the hardware object they must be defined here
    #   Missing units will raise an exception
    motor_x_unit: mm

    # Motor for fine translation (can be ommitted)
    motor_x_fine: pm1
    motor_x_fine_unit: um

    motor_y: m2
    motor_y_unit: mm

    motor_y_fine: pm2
    motor_y_fine_unit: um

    # Motor for z translation (currently unused)
    motor_z: samx
    motor_z_unit: um

    # Any addition motors that should be captured with a `subsample`
    additional:
      z: samx

    # The angle between the beam and the camera
    beamangle: 28
    
    # The size of the beam
    beamsize: [0.7e+3, 0.3e+3, 1.0e+3]

    # Whether this image source is the origin (almost always true)
    origin: true

    # Whether to view the camera downstream, or upstream
    downstream: true
```


### Video streaming
Video streaming is run through a separate service. There is an [MPEG](https://gitlab.esrf.fr/ui/video-streamer-mpeg) based streamer  and an [MJPEG](https://gitlab.esrf.fr/ui/video-streamer) streamer. The mpeg streamer is recommended as it offers lower latency and better compression.

## Scan Types
The scan types part of the configuration file maps specific `subsample` types to [Actors](actors.md). Valid `subsample` types are:

* roi (region)
* poi (point)
* loi (line)

This allows the user interface to be specific about which scans it proposes for each `subsample` type

```yaml
scantypes:
  roi:
    - roiscan
  poi:
    - poiscan
  loi:
    - loiscan
```

## Options
The options part of the configuration file allows various things to be en/disabled. Currently supported options are
```yaml
options:
  # show the limits of the translation motors
  polylines: true 
```

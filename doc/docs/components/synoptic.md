The synoptic component displays an svg layout of the beamline. The synoptic svg can be generated automatically using <https://gitlab.esrf.fr/ui/svg-synoptic>.

The configuration file defines the svg that will be sent to the client, and the mapping between the synoptic blocks.

An example config is as follows:
```yaml
synoptic: example.svg
elements:
- name: S1
  group: slits
  value: s1b.position
- ...
```

Each block of the synoptic can be mapped to a [Hardware Group](hardware.md#groups), i.e. in this case the element named `S1` will be mapped to the `slits` hardware group. If the status of this group is ok, then the block will marked as ok. The `value` attribute can be used to show a value above the synoptic block, this can be specified with dot notation `{object}.{property}`

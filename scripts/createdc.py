#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import argparse
import logging
from datetime import datetime

import h5py

from flask import Flask
from flask_apispec import FlaskApiSpec
from flask_socketio import SocketIO

from daiquiri.core.metadata import MetaData
from daiquiri.resources import utils

app = Flask(__name__)
docs = FlaskApiSpec(app)
socketio = SocketIO(app)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class DC:
    def __init__(self, metadata, session):
        self._metadata = metadata
        self._session = metadata.get_sessions(session=session, no_context=True)

    def create(
        self, sampleid=None, file=None, **kwargs,
    ):
        if not sampleid:
            raise Exception("Sampleid must be specified")

        if not kwargs.get("hdf5_data_path"):
            self.list_datasets(file)
            exit()

        if sampleid:
            sample = self._metadata.get_samples(sampleid=sampleid, no_context=True)

            if not sample:
                logger.error(f"No such sample {sampleid}")
                exit()

            abs_file = os.path.abspath(file)

            experimenttype = kwargs.get("experimenttype")
            if experimenttype is None:
                experimenttype = "experiment"

            datacollection = self._metadata.add_datacollection(
                imagedirectory=os.path.dirname(abs_file),
                filetemplate=os.path.basename(file),
                numberofimages=self.get_points(file, kwargs.get("hdf5_data_path")),
                sessionid=self._session["sessionid"],
                sampleid=sampleid,
                starttime=datetime.now(),
                endtime=datetime.now(),
                experimenttype=experimenttype,
                no_context=True,
            )

            self._metadata.update_datacollection(
                datacollectionid=datacollection["datacollectionid"],
                imagecontainersubpath=kwargs["hdf5_data_path"],
                wavelength=kwargs.get("wavelength"),
                exposuretime=kwargs.get("exposuretime"),
                endtime=datetime.now(),
                runstatus="Successful",
                comments="Import from createdc",
                no_context=True,
            )

            logger.info(f"Created datacollection {datacollection['datacollectionid']}")

    def get_points(self, file, hdf5_data_path):
        with h5py.File(file, mode="r") as h5:
            try:
                max_points = 0
                points = 0
                dataset = h5[hdf5_data_path]
                for k in dataset.keys():
                    points = len(dataset[k])
                    if points > max_points:
                        max_points = points

                return points

            except Exception:
                logger.exception(f"Couldnt read dataset {file}:{hdf5_data_path}")

    def list_datasets(self, file):
        datasets = []
        with h5py.File(file, mode="r") as h5:
            try:
                dss = h5["/"]
                for k in dss.keys():
                    datasets.append(k)

            except Exception:
                logger.exception("Couldnt read datasets")

        print("Found the following datasets:")
        for dataset in datasets:
            print(f"  {dataset}")

        exit()


def main():
    args = parse_args()
    utils.add_resource_root(args.resource_folder)

    config = utils.load_config("app.yml")
    metadata = MetaData(
        config, app=app, schema=None, docs=docs, socketio=socketio
    ).init()

    mff = DC(metadata, args.session)
    mff.create(
        args.sampleid,
        file=args.file,
        hdf5_data_path=args.hdf5_data_path,
        experimenttype=args.experimenttype,
        exposuretime=args.exposuretime,
        wavelength=args.wavelength,
    )


def parse_args():
    parser = argparse.ArgumentParser(
        description="Create a new dc pointing to an existing hdf5 file"
    )
    parser.add_argument(
        "--resource-folder",
        dest="resource_folder",
        help="Server resource directories",
        required=True,
    )

    parser.add_argument(
        "--session", required=True, help="The session to import the map in to",
    )

    parser.add_argument(
        "--sampleid", type=int, help="Sampleid to create dc for",
    )

    parser.add_argument(
        "--experimenttype", help="The experiment type",
    )

    parser.add_argument(
        "--wavelength", help="The experiment wavelength (A)",
    )

    parser.add_argument(
        "--exposuretime", help="The experiment exposuretime (s)",
    )

    parser.add_argument(
        "--hdf5-data-path", dest="hdf5_data_path", help="Data path in hdf5 file",
    )

    parser.add_argument("file", help="The hdf5 file")

    return parser.parse_args()


if __name__ == "__main__":
    main()

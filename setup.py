#!/usr/bin/env python
import sys
import re
import os
import subprocess
import logging

from setuptools import setup

TESTING = any(x in sys.argv for x in ["test", "pytest"])

# Stolen from bliss/setup.py
def generate_release_file():
    dirname = os.path.dirname(os.path.abspath(__file__))
    try:
        process = subprocess.run(
            ["git", "describe", "--tags", "--always"], capture_output=True, cwd=dirname
        )
        if process.returncode:
            raise Exception("Not a git repository")
    except Exception as e:
        logging.error(
            "Error while executing git to get version. Generating a dummy version instead",
            exc_info=True,
        )
        version = "0.0.0+master"

    else:
        version = process.stdout.strip().decode()
        # Normalize to PEP 440
        version = version.replace("-", "+", 1)
        # Incase there are no tags
        if not re.match(r"^\d+\.\d+\.\d+", version):
            version = "0.0.0+" + version

    name = "daiquiri"
    author = "BCU (ESRF)"
    author_email = ""
    license = "LGPLv3"
    copyright = "2015-2020 Beamline Control Unit, ESRF"
    description = "BeamLine Instrumentation Support Software"
    url = "ui.gitlab-pages.esrf.fr/daiquiri"

    src = f"""\
# -*- coding: utf-8 -*-
# Single source of truth for the version number and the like
import os
import re
import subprocess

dirname = os.path.dirname(os.path.abspath(__file__))

name = "{name}"
author = "{author}"
author_email = "{author_email}"
license = "{license}"
copyright = "{copyright}"
description = "{description}"
url = "{url}"
try:
    process = subprocess.run(
        ["git", "describe", "--tags", "--always"], capture_output=True, cwd=dirname
    )
    if process.returncode:
        raise Exception("Not a git repository")
except Exception as e:
    short_version = version = "{version}"
else:
    version = process.stdout.strip().decode()
    # Normalize to PEP 440
    version = version.replace("-", "+", 1)
    # Incase there are no tags
    if not re.match(r"^\d+\.\d+\.\d+", version):
        version = "0.0.0+" + version
    short_version = version

_package_version = version.split("+", 1)[0]
version_info = [int(v) for v in _package_version.split(".")]
"""
    with open(os.path.join(dirname, "daiquiri", "release.py"), "w") as f:
        f.write(src)
    return locals()


def main():
    py = sys.version_info
    py_str = ".".join(map(str, py))

    if py < (3,):
        print(("Incompatible python version ({0}). Needs python 3.x ".format(py_str)))
        sys.exit(1)

    meta = generate_release_file()

    tests_require = ["pytest", "pytest-cov"]

    setup_requires = []
    if TESTING:
        setup_requires += ["pytest-runner"]

    setup(
        name=meta["name"],
        author=meta["author"],
        version=meta["version"],
        description=meta["description"],
        license=meta["license"],
        url=meta["url"],
        package_dir={"daiquiri": "daiquiri"},
        tests_require=tests_require,
        test_suite="tests",
        entry_points={"console_scripts": ["daiquiri-server = daiquiri.app:main"]},
    )


if __name__ == "__main__":
    main()
